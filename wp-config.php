<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://ru.wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define( 'DB_NAME', 'lexfort' );

/** Имя пользователя MySQL */
define( 'DB_USER', 'mysql' );

/** Пароль к базе данных MySQL */
define( 'DB_PASSWORD', 'mysql' );

/** Имя сервера MySQL */
define( 'DB_HOST', 'localhost' );

/** Кодировка базы данных для создания таблиц. */
define( 'DB_CHARSET', 'utf8mb4' );

/** Схема сопоставления. Не меняйте, если не уверены. */
define( 'DB_COLLATE', '' );

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '#|HYGO1(XN%blh_+T$Ha!uRTqf%vBl6Pro-ocifY.I[3HX4^#){tDHAV@evP%05L' );
define( 'SECURE_AUTH_KEY',  '5Ojin*k&T<EeZ2SA.)fD|C2(Th@I_D,wwLp+Vd{a-:a<xGImR)C}iGmoB-qC,z!J' );
define( 'LOGGED_IN_KEY',    'nIr1(M5GnN)B)=)2?QkpC6;NH*Gwl$d~lk%94%d]oi>JFO6`r%FmoM)S5ojw!=Xt' );
define( 'NONCE_KEY',        'p(eW4pHK+frU:/d@.Ei}k+_sEu(,@8k}G5!Ux+>uHrZ-5VJ]q ]F&pM}7L#2^QxB' );
define( 'AUTH_SALT',        'Plw2lnsUa-N4D?GKdqy-:$)/4BSb1T6Y>|g5?bRN:GBSU=6GCtacoI(6UXN0}HvQ' );
define( 'SECURE_AUTH_SALT', 'rL`+6GY19gxQ$v:+7UM&LY*(<tFKVnz?ZU]a@R3e`!L8k2rCe!E|yfQ?,o{4uO2t' );
define( 'LOGGED_IN_SALT',   'aIs,W`~JuCZ+P:X[.Of 0%h]R?1&|->;>:3yR !iVT>e-?Wz3P)ws+&-KP#xLh>Q' );
define( 'NONCE_SALT',       '<@f^Ya%GA ,Gmf+#j&?zm(Yj_933Wzf*dfoB2n8v[Z.ZG9LyJ:_Yz*/CC/dLx}eD' );

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 *
 * Информацию о других отладочных константах можно найти в документации.
 *
 * @link https://ru.wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Инициализирует переменные WordPress и подключает файлы. */
require_once ABSPATH . 'wp-settings.php';