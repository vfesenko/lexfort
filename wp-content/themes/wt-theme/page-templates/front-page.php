<?php
/**
 * Template Name: Front Page
 *
 */

get_header();
global $post; ?>

<section class="main-banner flex">
	<div class="main-banner__container container flex">
		<div class="main-banner__info flex flex-dc">
			<ul class="header-top-row__citys-list flex">
				<li class="header-top-row__citys-item flex">
					<a href="#" class="header-top-row__citys-link header-top-row__citys-link--active flex flex-alf">
						<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
							<path d="M15.4002 16.2628L15.4001 16.2627C15.3323 16.2525 15.2672 16.2289 15.2085 16.1932C15.1498 16.1576 15.0987 16.1106 15.058 16.055C15.0173 15.9995 14.9879 15.9364 14.9714 15.8694C14.9549 15.8025 14.9516 15.7329 14.9617 15.6646C14.9718 15.5963 14.9951 15.5307 15.0303 15.4715C15.0655 15.4123 15.112 15.3606 15.167 15.3195C15.2221 15.2784 15.2846 15.2485 15.3511 15.2318L15.4002 16.2628ZM15.4002 16.2628C16.8884 16.486 18.1361 16.8432 18.9723 17.2576C19.3899 17.4646 19.6954 17.6877 19.8803 17.8849C20.0654 18.0823 20.1195 18.2311 20.1195 18.3524C20.1195 18.519 20.0119 18.7482 19.6318 19.0358C19.2537 19.322 18.646 19.6149 17.8768 19.8611C16.3391 20.3533 14.1591 20.6717 11.7474 20.6717C9.33578 20.6717 7.15568 20.3533 5.61802 19.8611C4.8488 19.6149 4.24112 19.322 3.86298 19.0358C3.48289 18.7482 3.37532 18.519 3.37532 18.3524C3.37532 18.2311 3.4294 18.0823 3.61453 17.8849C3.79943 17.6877 4.10494 17.4646 4.52254 17.2576C5.35842 16.8433 6.60554 16.4862 8.09308 16.263C8.22856 16.2492 8.35321 16.1825 8.44053 16.0773C8.52818 15.9717 8.57124 15.8357 8.56064 15.6985C8.55003 15.5613 8.48657 15.4337 8.38369 15.343C8.28093 15.2525 8.14704 15.2062 8.01072 15.2141M15.4002 16.2628L7.93926 15.2221M8.01072 15.2141C7.98675 15.2151 7.96287 15.2178 7.93926 15.2221M8.01072 15.2141C8.01052 15.2141 8.01031 15.2142 8.01011 15.2142L8.01391 15.278L8.01133 15.2141C8.01112 15.2141 8.01092 15.2141 8.01072 15.2141ZM7.93926 15.2221C6.37863 15.4563 5.05047 15.8276 4.06679 16.3151C3.57459 16.559 3.16589 16.8363 2.85868 17.1638C2.551 17.4919 2.33551 17.9016 2.33551 18.3524C2.33551 18.9708 2.72335 19.4851 3.24435 19.8793C3.76616 20.2742 4.46134 20.5924 5.30118 20.8612C6.98181 21.3992 9.24816 21.7211 11.7474 21.7211C14.2467 21.7211 16.513 21.3992 18.1936 20.8612C19.0335 20.5924 19.7287 20.2742 20.2505 19.8793C20.7715 19.4851 21.1593 18.9708 21.1593 18.3524C21.1593 17.9016 20.9438 17.4919 20.6362 17.1638C20.3289 16.8363 19.9202 16.559 19.428 16.3151C18.4442 15.8275 17.1156 15.4561 15.5546 15.222L7.93926 15.2221ZM11.7474 1.31805C8.61907 1.31805 6.06188 3.85045 6.06188 6.96916C6.06188 9.43389 7.18163 11.7948 8.37163 13.884C8.74213 14.5344 9.12206 15.1642 9.4814 15.7598C9.69887 16.1202 9.9088 16.4682 10.1045 16.8007C10.6253 17.6852 11.0458 18.4608 11.252 19.0848C11.2854 19.1907 11.3512 19.2832 11.4402 19.3489C11.5293 19.4148 11.6369 19.4503 11.7474 19.4503C11.858 19.4503 11.9656 19.4148 12.0546 19.3489C12.1436 19.2832 12.2095 19.1907 12.2429 19.0848C12.449 18.4608 12.8696 17.6852 13.3903 16.8007C13.586 16.4682 13.796 16.1202 14.0134 15.7598C14.3728 15.1642 14.7527 14.5344 15.1232 13.884C16.3132 11.7948 17.4329 9.43389 17.4329 6.96916C17.4329 3.85045 14.8758 1.31805 11.7474 1.31805ZM11.7474 2.36746C14.3221 2.36746 16.3931 4.4292 16.3931 6.96916C16.3931 9.1121 15.3828 11.328 14.2213 13.3671C13.8708 13.9824 13.5107 14.5752 13.1639 15.1462C13.0576 15.3211 12.9526 15.4939 12.8496 15.6648C12.4366 16.349 12.0543 17.0005 11.7474 17.6171C11.4406 17.0005 11.0582 16.349 10.6453 15.6648C10.5422 15.494 10.4372 15.3211 10.331 15.1462C9.98413 14.5752 9.62402 13.9824 9.27357 13.3671C8.11206 11.328 7.10169 9.1121 7.10169 6.96916C7.10169 4.4292 9.17271 2.36746 11.7474 2.36746ZM11.7474 3.88125C10.0447 3.88125 8.64826 5.26524 8.64826 6.96916C8.64826 8.67327 10.0449 10.0499 11.7474 10.0499C13.4499 10.0499 14.8466 8.67327 14.8466 6.96916C14.8466 5.26524 13.4501 3.88125 11.7474 3.88125ZM11.7474 4.93066C12.8963 4.93066 13.8068 5.84377 13.8068 6.96916C13.8068 8.09426 12.8966 9.00045 11.7474 9.00045C10.5983 9.00045 9.68807 8.09426 9.68807 6.96916C9.68807 5.84377 10.5986 4.93066 11.7474 4.93066Z" stroke-width="0.127811"/>
						</svg>
						<p>Омск</p>
					</a>
				</li>
				<li class="header-top-row__citys-item flex">
					<a href="#" class="header-top-row__citys-link flex flex-alf">
						<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
							<path d="M15.4002 16.2628L15.4001 16.2627C15.3323 16.2525 15.2672 16.2289 15.2085 16.1932C15.1498 16.1576 15.0987 16.1106 15.058 16.055C15.0173 15.9995 14.9879 15.9364 14.9714 15.8694C14.9549 15.8025 14.9516 15.7329 14.9617 15.6646C14.9718 15.5963 14.9951 15.5307 15.0303 15.4715C15.0655 15.4123 15.112 15.3606 15.167 15.3195C15.2221 15.2784 15.2846 15.2485 15.3511 15.2318L15.4002 16.2628ZM15.4002 16.2628C16.8884 16.486 18.1361 16.8432 18.9723 17.2576C19.3899 17.4646 19.6954 17.6877 19.8803 17.8849C20.0654 18.0823 20.1195 18.2311 20.1195 18.3524C20.1195 18.519 20.0119 18.7482 19.6318 19.0358C19.2537 19.322 18.646 19.6149 17.8768 19.8611C16.3391 20.3533 14.1591 20.6717 11.7474 20.6717C9.33578 20.6717 7.15568 20.3533 5.61802 19.8611C4.8488 19.6149 4.24112 19.322 3.86298 19.0358C3.48289 18.7482 3.37532 18.519 3.37532 18.3524C3.37532 18.2311 3.4294 18.0823 3.61453 17.8849C3.79943 17.6877 4.10494 17.4646 4.52254 17.2576C5.35842 16.8433 6.60554 16.4862 8.09308 16.263C8.22856 16.2492 8.35321 16.1825 8.44053 16.0773C8.52818 15.9717 8.57124 15.8357 8.56064 15.6985C8.55003 15.5613 8.48657 15.4337 8.38369 15.343C8.28093 15.2525 8.14704 15.2062 8.01072 15.2141M15.4002 16.2628L7.93926 15.2221M8.01072 15.2141C7.98675 15.2151 7.96287 15.2178 7.93926 15.2221M8.01072 15.2141C8.01052 15.2141 8.01031 15.2142 8.01011 15.2142L8.01391 15.278L8.01133 15.2141C8.01112 15.2141 8.01092 15.2141 8.01072 15.2141ZM7.93926 15.2221C6.37863 15.4563 5.05047 15.8276 4.06679 16.3151C3.57459 16.559 3.16589 16.8363 2.85868 17.1638C2.551 17.4919 2.33551 17.9016 2.33551 18.3524C2.33551 18.9708 2.72335 19.4851 3.24435 19.8793C3.76616 20.2742 4.46134 20.5924 5.30118 20.8612C6.98181 21.3992 9.24816 21.7211 11.7474 21.7211C14.2467 21.7211 16.513 21.3992 18.1936 20.8612C19.0335 20.5924 19.7287 20.2742 20.2505 19.8793C20.7715 19.4851 21.1593 18.9708 21.1593 18.3524C21.1593 17.9016 20.9438 17.4919 20.6362 17.1638C20.3289 16.8363 19.9202 16.559 19.428 16.3151C18.4442 15.8275 17.1156 15.4561 15.5546 15.222L7.93926 15.2221ZM11.7474 1.31805C8.61907 1.31805 6.06188 3.85045 6.06188 6.96916C6.06188 9.43389 7.18163 11.7948 8.37163 13.884C8.74213 14.5344 9.12206 15.1642 9.4814 15.7598C9.69887 16.1202 9.9088 16.4682 10.1045 16.8007C10.6253 17.6852 11.0458 18.4608 11.252 19.0848C11.2854 19.1907 11.3512 19.2832 11.4402 19.3489C11.5293 19.4148 11.6369 19.4503 11.7474 19.4503C11.858 19.4503 11.9656 19.4148 12.0546 19.3489C12.1436 19.2832 12.2095 19.1907 12.2429 19.0848C12.449 18.4608 12.8696 17.6852 13.3903 16.8007C13.586 16.4682 13.796 16.1202 14.0134 15.7598C14.3728 15.1642 14.7527 14.5344 15.1232 13.884C16.3132 11.7948 17.4329 9.43389 17.4329 6.96916C17.4329 3.85045 14.8758 1.31805 11.7474 1.31805ZM11.7474 2.36746C14.3221 2.36746 16.3931 4.4292 16.3931 6.96916C16.3931 9.1121 15.3828 11.328 14.2213 13.3671C13.8708 13.9824 13.5107 14.5752 13.1639 15.1462C13.0576 15.3211 12.9526 15.4939 12.8496 15.6648C12.4366 16.349 12.0543 17.0005 11.7474 17.6171C11.4406 17.0005 11.0582 16.349 10.6453 15.6648C10.5422 15.494 10.4372 15.3211 10.331 15.1462C9.98413 14.5752 9.62402 13.9824 9.27357 13.3671C8.11206 11.328 7.10169 9.1121 7.10169 6.96916C7.10169 4.4292 9.17271 2.36746 11.7474 2.36746ZM11.7474 3.88125C10.0447 3.88125 8.64826 5.26524 8.64826 6.96916C8.64826 8.67327 10.0449 10.0499 11.7474 10.0499C13.4499 10.0499 14.8466 8.67327 14.8466 6.96916C14.8466 5.26524 13.4501 3.88125 11.7474 3.88125ZM11.7474 4.93066C12.8963 4.93066 13.8068 5.84377 13.8068 6.96916C13.8068 8.09426 12.8966 9.00045 11.7474 9.00045C10.5983 9.00045 9.68807 8.09426 9.68807 6.96916C9.68807 5.84377 10.5986 4.93066 11.7474 4.93066Z" stroke-width="0.127811"/>
						</svg>
						<p>Москва</p>
					</a>
				</li>
			</ul>

			<h1 class="main-banner__title">
				<p><?php the_field('banner-title') ?></p>
			</h1>

			<div class="main-banner__text flex flex-dc">
				<?php the_field('banner-text') ?>
			</div>

			<div class="main-banner__button flex">
				<a href="#form-question" class="main-banner__link all-button-body flex">Задать вопрос</a>
				<a href="#" class="main-banner__link-bot all-button flex flex-alc">
					<p>Чат-бот</p>
					<svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
						<path d="M10 4L14.0938 8L10 12" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
						<path d="M13.3125 8H1.90625" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
					</svg>
				</a>
			</div>
		</div>
		<div class="main-banner__photos flex">
			<div class="main-banner__photos-bg flex">
				<?php $banner_bg = get_field('banner-block-img-bg'); ?>
				<img src="<?php echo $banner_bg['url']; ?>" alt="<?php echo $banner_bg['title']; ?>">
			</div>
			<div class="main-banner__position-wrap">
				<div class="main-banner__photos-images flex">
					<?php $banner_photo = get_field('banner-block-photo'); ?>
					<img src="<?php echo $banner_photo['url']; ?>" alt="<?php echo $banner_photo['title']; ?>">
				</div>
				<div class="main-banner__photos-bubble flex flex-dc flex-jcc flex-alc">
					<p class="main-banner__photos-text">Работаем с</p>
					<p class="main-banner__photos-ear">2004</p>
					<p class="main-banner__photos-text">года</p>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="main-services flex">
	<div class="main-services__container container flex flex-alc">
		<div class="main-services__info flex flex-dc">
			<h2 class="main-services__title all-title flex">
				<?php the_field('main-services-title') ?>
			</h2>
			<div class="main-services__text flex">
				<?php the_field('main-services-text') ?>
			</div>
			<a href="/services/" class="main-services__link all-button flex flex-alc">
				<p>Все услуги</p>
				<svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
					<path d="M10 4L14.0938 8L10 12" stroke="#00935C" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
					<path d="M13.3125 8H1.90625" stroke="#00935C" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
				</svg>
			</a>
		</div>

		<div class="main-services__content">
			<? if(have_rows('main-services-list')): ?>
				<ul class="main-services__list flex">
					<?php while (have_rows('main-services-list')): the_row();
						$social_link = get_sub_field('main-services-item-link');
						$social_link_text = get_sub_field('main-services-item-link-text');
						$social_icon = get_sub_field('main-services-icon');
						$social_info = get_sub_field('main-services-info');
					?>
						<li class="main-services__item flex flex-dc">
							<div class="main-services__item-icon">
								<img src="<?php echo $social_icon['url']; ?>" alt="<?php echo $social_icon['title']; ?>">
							</div>
							<a href="<?php echo $social_link_text; ?>" class="main-services__item-link flex flex-alc">
								<p><?php echo $social_link; ?></p>
								<svg width="25" height="16" viewBox="0 0 25 16" fill="none">
									<path d="M18 3L23.0938 8L18 13" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
									<path d="M22 8L2 8" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
								</svg>
							</a>
							<div class="main-services__item-text">
								<?php echo $social_info; ?>
							</div>
						</li>
					<?php endwhile; ?>
				</ul>
			<?php endif; ?>
		</div>
	</div>
</section>

<section class="main-rating flex">
	<div class="main-rating__container container flex flex-dc">
		<h2 class="main-rating__title all-title flex flex-dc">
			Рейтинги
		</h2>
		<div class="main-rating__content">
			<? if(have_rows('rating-list')): ?>
					<ul class="main-rating__list flex flex-alc">
						<?php while (have_rows('rating-list')): the_row();
							$rating_icon = get_sub_field('rating-icon');
						?>
							<li class="main-rating__item flex flex-jcc flex-alc">
								<img src="<?php echo $rating_icon['url']; ?>" alt="<?php echo $rating_icon['title']; ?>">
							</li>
						<?php endwhile; ?>
					</ul>
				<?php endif; ?>
		</div>
	</div>
</section>

<?php $employees_bg = get_field('employees-bg'); ?>
<section class="main-employees flex"  style="background-image: url(<?php echo $employees_bg['url']; ?>);">
	<div class="main-employees__container container flex flex-dc">
		<h2 class="main-employees__title flex flex-dc">
			Команда
		</h2>
		<div class="main-employees__content-wrap flex">
			<div class="main-employees__content-texts">
				<? if(have_rows('employees-list')): ?>
					<ul class="main-employees__list js-main-employees__list flex flex-alc">
						<?php while (have_rows('employees-list')): the_row();
							$employees_fio = get_sub_field('employees-fio');
							$employees_position = get_sub_field('employees-position');
							$employees_office = get_sub_field('employees-office');
							$employees_info = get_sub_field('employees-info');
						?>
							<li class="main-employees__item flex flex-jcc flex-alc">
								<div class="main-employees__fio"><?php echo $employees_fio; ?></div>
								<div class="main-employees__info flex flex-als">
									<div class="main-employees__position"><?php echo $employees_position; ?></div>
									<div class="main-employees__fish">
										<svg width="8" height="8" viewBox="0 0 8 8" fill="none">
											<circle cx="4" cy="4" r="4" fill="white"/>
										</svg>
									</div>
									<div class="main-employees__office"><?php echo $employees_office; ?></div>
								</div>
								<div class="main-employees__text"><?php echo $employees_info; ?></div>
							</li>
						<?php endwhile; ?>
					</ul>
					<div class="pagination-slider-wrap__content flex aic">
						<div class="slider-arrow-prev js-employees-slider-arrow-prev">
							<svg width="25" height="16" viewBox="0 0 25 16" fill="none">
								<path d="M7 3L1.90625 8L7 13" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
								<path d="M3 8L23 8" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
							</svg>
						</div>
						<div class="sl-count"><span class="sl-count__current banner-sl-count__current">1</span> / <span class="sl-count__total banner-sl-count__total"></span></div>
						<div class="slider-arrow-next js-employees-slider-arrow-next">
							<svg width="25" height="16" viewBox="0 0 25 16" fill="none">
								<path d="M18 3L23.0938 8L18 13" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
								<path d="M22 8L2 8" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
							</svg>
						</div>
					</div>
				<?php endif; ?>
			</div>

			<div class="main-employees__content-photos flex flex-alf">
				<? if(have_rows('employees-list')): ?>
					<ul class="main-employees__list-photo js-main-employees__list-photo flex flex-alc">
						<?php while (have_rows('employees-list')): the_row();
							$employees_photo = get_sub_field('employees-photo');
						?>
							<li class="main-employees__photo-item js-main-employees__photo-item flex flex-jcc flex-alf">
								<img src="<?php echo $employees_photo['url']; ?>" alt="<?php echo $employees_photo['title']; ?>">
							</li>
						<?php endwhile; ?>
					</ul>
				<?php endif; ?>
			</div>
		</div>
	</div>
</section>

<section class="main-news-analitics">
	<div class="main-news-analitics__container container flex">
		<div class="main-news flex flex-dc">
			<div class="main-news-header flex flex-alc">
				<h2 class="main-news__title all-title">Новости</h2>
				<a class="main-news-analitics all-button  flex flex-alc">
					<p>Все новости</p>
					<svg width="16" height="16" viewBox="0 0 16 16" fill="none">
						<path d="M10 4L14.0938 8L10 12" stroke="#00935C" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
						<path d="M13.3125 8H1.90625" stroke="#00935C" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
					</svg>
				</a>
			</div>
			<ul class="main-news__list flex flex-dc">
				<?php 
					$posts = new WP_Query([
						'post_type'				=> 'post_news',
						'posts_per_page'	=> 4,
						'order_by'				=> 'date',
						'order'						=> 'desc',
					]);
				?>
				<?php if($posts->have_posts()): ?>
					<?php while($posts->have_posts()) : $posts->the_post(); ?>
						<?php 
						$desc = get_the_content(); 
						$new_images = get_field('news-images')
						?>
						<li class="main-news__item flex">
							<div class="main-news__image flex flex-jcc flex-alc">
								<img src="<?php echo $new_images['sizes']['home_news_img']; ?>" alt="">
							</div>
							<div class="main-news__item-content flex flex-dc">
								<p class="main-news__item-date"><?php echo get_the_date('d F Y'); ?></p>
								<a href="<?php echo get_permalink(); ?>" class="main-news__link"><?php the_title(); ?></a>
							</div>
						</li>
					<?php endwhile; ?>
					<?php wp_reset_postdata(); ?>
				<?php endif; ?>
			</ul>
		</div>
		<div class="main-news main-analitics flex flex-dc">
			<div class="main-news-header flex flex-alc">
				<h2 class="main-news__title all-title">Аналитика</h2>
				<a class="main-news-analitics all-button  flex flex-alc">
					<p>Вся аналитика</p>
					<svg width="16" height="16" viewBox="0 0 16 16" fill="none">
						<path d="M10 4L14.0938 8L10 12" stroke="#00935C" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
						<path d="M13.3125 8H1.90625" stroke="#00935C" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
					</svg>
				</a>
			</div>
			<ul class="main-news__list flex flex-dc">
				<?php 
					$posts = new WP_Query([
						'post_type'				=> 'post_analitics',
						'posts_per_page'	=> 4,
						'order_by'				=> 'date',
						'order'						=> 'desc',
					]);
				?>
				<?php if($posts->have_posts()): ?>
					<?php while($posts->have_posts()) : $posts->the_post(); ?>
						<?php 
						$desc = get_the_content(); 
						$new_images = get_field('analitics-icon')
						?>
						<li class="main-news__item flex">
							<div class="main-news__image flex flex-jcc flex-alc">
								<img src="<?php echo $new_images['url']; ?>" alt="">
							</div>
							<div class="main-news__item-content flex flex-dc">
								<p class="main-news__item-date"><?php echo get_the_date('d F Y'); ?></p>
								<a href="<?php echo get_permalink(); ?>" class="main-news__link"><?php the_title(); ?></a>
							</div>
						</li>
					<?php endwhile; ?>
					<?php wp_reset_postdata(); ?>
				<?php endif; ?>
			</ul>
		</div>
	</div>
</section>

<?php $video_bg = get_field('video-block-bg'); ?>
<section class="main-video flex"  style="background-image: url(<?php echo $video_bg['url']; ?>);">
	<div class="main-video__container container flex flex-dc">
		<h2 class="main-video__title">Видео</h2>
		<div class="main-video__header flex flex-alc">
			<h3 class="main-video__header-title all-title">
				Наш Youtube-канал
			</h3>
			<a href="#" class="main-video__header-link flex flex-alc all-button">
				<p>Больше видео</p>
				<svg width="16" height="16" viewBox="0 0 16 16" fill="none">
					<path d="M10 4L14.0938 8L10 12" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
					<path d="M13.3125 8H1.90625" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
				</svg>
			</a>
		</div>
		<div class="main-video__content">
			<ul class="main-video__list flex">
				<?php 
					$posts = new WP_Query([
						'post_type'				=> 'post_video',
						'posts_per_page'	=> 4,
						'order_by'				=> 'date',
						'order'						=> 'desc',
					]);
				?>
				<?php if($posts->have_posts()): ?>
					<?php while($posts->have_posts()) : $posts->the_post(); ?>
						<?php 
						$desc = get_the_content(); 
						$video_images = get_field('video-item-bg');
						$video_link = get_field('youtube-link');
						$video_time = get_field('video-time');
						?>
						<li class="main-video__item flex">
							<a href="<?php echo $video_link ?>" class="main-video__link  flex" target="_blank">
								<div class="main-video__image flex flex-jcc flex-alc">
									<img src="<?php echo $video_images['url']; ?>" alt="">
								</div>
								<div class="main-video__item-content flex flex-dc">
									<svg width="48" height="55" viewBox="0 0 48 55" fill="none">
										<path d="M45.5709 30.552C48.2135 28.9714 48.1541 25.1229 45.464 23.6246L6.12146 1.71197C3.43137 0.213674 0.12811 2.18935 0.175591 5.26818L0.870007 50.2962C0.917488 53.375 4.2801 55.2479 6.92271 53.6673L45.5709 30.552Z" fill="white"/>
									</svg>

									<p class="main-video__item-title"><?php the_title(); ?></p>

									<div class="main-video__item-time-wrap flex flex-alc">
										<svg width="15" height="15" viewBox="0 0 15 15" fill="none">
											<g clip-path="url(#clip0)">
											<path d="M10.3145 7.96875H7.50195C7.2432 7.96875 7.0332 7.75875 7.0332 7.5C7.0332 7.24125 7.2432 7.03125 7.50195 7.03125H10.3145C10.5732 7.03125 10.7832 7.24125 10.7832 7.5C10.7832 7.75875 10.5732 7.96875 10.3145 7.96875Z" fill="white"/>
											<path d="M7.50195 7.96875C7.2432 7.96875 7.0332 7.75875 7.0332 7.5V4.6875C7.0332 4.42875 7.2432 4.21875 7.50195 4.21875C7.7607 4.21875 7.9707 4.42875 7.9707 4.6875V7.5C7.9707 7.75875 7.7607 7.96875 7.50195 7.96875Z" fill="white"/>
											<path d="M7.48234 13.5846C7.48211 13.5846 7.48211 13.5846 7.48187 13.5846C6.94914 13.5846 6.42062 13.5152 5.91132 13.3781C3.71687 12.7877 1.9982 10.9697 1.5332 8.74617C1.05976 6.48094 1.93421 4.09969 3.76093 2.67961C4.81726 1.85859 6.14922 1.40625 7.51187 1.40625C8.45523 1.40625 9.39203 1.62563 10.2212 2.04023C12.2685 3.06469 13.5918 5.205 13.5918 7.4932C13.5918 9.36023 12.7108 11.1598 11.2352 12.3068C10.1753 13.1309 8.84218 13.5846 7.48234 13.5846ZM7.51164 2.34375C6.3564 2.34375 5.22859 2.72578 4.33609 3.41977C2.78992 4.62141 2.05 6.63703 2.45078 8.55422C2.84406 10.4351 4.29812 11.9733 6.15484 12.4727C6.58468 12.5883 7.03117 12.6471 7.48211 12.6471C7.48211 12.6471 7.48211 12.6471 7.48234 12.6471C8.635 12.6471 9.76328 12.2634 10.6598 11.5666C11.9085 10.5959 12.6543 9.07289 12.6543 7.4932C12.6543 5.55703 11.5347 3.74578 9.80171 2.87883C9.10234 2.52891 8.31039 2.34375 7.51164 2.34375Z" fill="white"/>
											</g>
											<defs>
											<clipPath id="clip0">
											<rect width="15" height="15" fill="white"/>
											</clipPath>
											</defs>
										</svg>

										<p class="main-video__item-time">
											<?php echo $video_time; ?>
										</p>
									</div>
								</div>
							</a>
						</li>
					<?php endwhile; ?>
					<?php wp_reset_postdata(); ?>
				<?php endif; ?>
			</ul>
		</div>
	</div>
</section>

<section class="main-smi flex">
	<div class="main-smi__container container flex flex-dc">
		<div class="main-smi__header flex flex-alc">
			<h2 class="main-smi__header-title all-title">
				СМИ о нас
			</h2>
			<a href="#" class="main-smi__header-link flex flex-alc all-button">
				<p>Все статьи</p>
				<svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
					<path d="M10 4L14.0938 8L10 12" stroke="#00935C" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
					<path d="M13.3125 8H1.90625" stroke="#00935C" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
				</svg>
			</a>
		</div>
		<div class="main-smi__content">
			<ul class="main-smi__list flex">
				<?php 
					$posts = new WP_Query([
						'post_type'				=> 'post_smi',
						'posts_per_page'	=> 4,
						'order_by'				=> 'date',
						'order'						=> 'desc',
					]);
				?>
				<?php if($posts->have_posts()): ?>
					<?php while($posts->have_posts()) : $posts->the_post(); ?>
						<?php 
						$desc = get_the_content(); 
						$new_images = get_field('smi-icon')
						?>
						<li class="main-smi__item flex flex-dc">
							<div class="main-smi__image flex flex-jcc flex-alc">
								<img src="<?php echo $new_images['url']; ?>" alt="">
							</div>
							<div class="main-smi__item-content flex flex-dc">
								<p class="main-smi__item-date"><?php echo get_the_date('d F Y'); ?></p>
								<a href="<?php echo get_permalink(); ?>" class="main-smi__link"><?php the_title(); ?></a>
							</div>
						</li>
					<?php endwhile; ?>
					<?php wp_reset_postdata(); ?>
				<?php endif; ?>
			</ul>
		</div>
	</div>
</section>

<?php $main_form_bg = get_field('main-form-bg'); ?>
<section class="main-form flex"  style="background-image: url(<?php echo $main_form_bg['url']; ?>);">
	<div id="form-question" class="main-form__container container flex flex-dc flex-alc">
		<h3 class="main-form__title">Задать вопрос</h3>
		<?php echo do_shortcode('[contact-form-7 id="5" title="Контактная форма 1"]'); ?>
	</div>
</section>





<?php
get_footer();




