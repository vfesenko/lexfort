<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 */

 /*
Template Name: Шаблон страницы новости
Template Post Type: page
*/
global $posts;
get_header(); ?>


<div class="general-page">
    <section class="general-page__header container">
        <div class="general-page__header-container container">
            <div class="breadcrumb flex flex-jcs flex-alc"><?php get_breadcrumb(); ?></div>
            <h1 class="general-page__header-title">Новости</h1>
            <nav class="general-page__header-nav">
                <ul class="general-page__header-nav-list">
                    <li class="general-page__header-nav-list">
                        <a href="/news/" class="general-page__header-nav-item">Новости</a>
                    </li>
                    <li class="general-page__header-nav-list">
                        <a href="/smi/" class="general-page__header-nav-item">Сми</a>
                    </li>
                    <li class="general-page__header-nav-list">
                        <a href="/analitics/" class="general-page__header-nav-item">Аналитика</a>
                    </li>
                </ul>
            </nav>
        </div>
    </section>
</div>










<div class="page-news flex flex-dc flex-alc">
    <div class="page-news__header flex flex-jcc w-100">
        <div class="page-news__header-background flex flex-dc w-100">
            <?php $form_bg_img = get_field('izobrazhenie_fona_pervogo_bloka'); ?>
            <img src="<?php echo $form_bg_img['url']; ?>" alt="<?php echo $form_bg_img['title']; ?>">
        </div>
        <div class="page-news__header-container container flex flex-dc">
            <h2 class="page-news__header-title">Новости</h2>
            <div class="page-news__header-info">
                <?php the_field('opisanie_novostej_pod_zagolovkom'); ?>
            </div>
        </div>
    </div>

    <div class="page-news__content-container container flex flex-jcc w-100">
        <div class="page-news__content w100">
            <ul class="page-news__content-list js-page-news__content-list flex">
                <?php 
                    $posts = new WP_Query([
                        'post_type'				=> 'post',
                        'posts_per_page'	=> 4,
                        'category_name'		=> 'novosti',
                        'order_by'				=> 'date',
                        'order'						=> 'desc',
                    ]);
                ?>
                <?php if($posts->have_posts()): ?>
                    <?php while($posts->have_posts()) : $posts->the_post(); ?>
                        <?php $desc = get_the_content(); ?>
                        <li class="news__content-item js-page-news__content-item flex">
                            <div class="news__content-image flex jcc aic">
                                <img src="<?php echo get_the_post_thumbnail_url($posts->ID, 'post_news_img'); ?>" alt="">
                            </div>
                            <div class="news__content-info flex">
                                <b><?php the_title(); ?></b>
                                <a href="<?php echo get_permalink(); ?>" class="news__content-link flex aic">
                                    <p>Читать статью</p>
                                    <div class="news__content-icon flex jcc aic">
                                        <svg width="18" height="14" viewBox="0 0 18 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M10.375 0.125L9.39187 1.08269L14.6031 6.3125H0.75V7.6875H14.6031L9.39187 12.8939L10.375 13.875L17.25 7L10.375 0.125Z" fill="#141414"/>
                                        </svg>
                                    </div>
                                </a>
                            </div>
                        </li>
                    <?php endwhile; ?>
                    <?php wp_reset_postdata(); ?>

                    <?php else : ?>

                    <b class="news__none-title all-title">В настоящий момент новостей нет</b>
                <?php endif; ?>
            </ul>
        </div>

        <div class="page-project__link-show-more-wrap flex flex-jcc w100">
            <a href="#" class="page-project__link-show-more all-button flex flex-alc js-page-news__link-show-more">
                <p>Показать ещё</p>
            </a>
        </div>
    </div>
</div>



<?php
get_footer();